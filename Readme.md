# Installation

### copie des fichiers de config 

```bash 
cp -rvf .vim .vimrc ~/
```

Ouvrir Vim

```bash
<ESC> :PlugInstall
```



### installation de ycm :

``` bash
cd .vim/plugged/YouCompleteMe
python install.py --all
```

