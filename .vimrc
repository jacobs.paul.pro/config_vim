" Avoid compatibility with old VI
set nocompatible

" source template
" source /usr/share/vim/vim81/defaults.vim

" Load settings in config folder
runtime! config/**/*.vim

map <F1> :SyntasticCheck<CR>

set list listchars=nbsp:¤,tab:··,trail:¤,extends:▶,precedes:◀ " ,eol:¶


let mapleader =","


if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.}vim/autoload/plug.vim"'))
    echo "Downloading junegunn/vim-plug to manage plugins..."
    silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.}vim/autoload/
    silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.}vim/autoload/plug.vim
    autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.}vim/plugged"'))
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'junegunn/goyo.vim'
Plug 'jreybert/vimagit'
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-syntastic/syntastic'
Plug 'majutsushi/tagbar'
Plug 'tpope/vim-commentary'
Plug 'kovetskiy/sxhkd-vim'
Plug 'ap/vim-css-color'
Plug 'jiangmiao/auto-pairs'
Plug 'neomake/neomake'
Plug 'Valloric/YouCompleteMe'
call plug#end()

" basic config
    nnoremap c "_c
    filetype plugin on
    syntax on
    set encoding=utf-8
    set number relativenumber

" Syntastic props
    " set statusline+=%#warningmsg#
    " set statusline+=%{SyntasticStatuslineFlag()}
    " set statusline+=%*
    

    " let g:syntastic_always_populate_loc_list = 1
    " let g:syntastic_auto_loc_list = 1
    let g:syntastic_check_on_open = 1
    let g:syntastic_check_on_wq = 0
    let g:syntastic_yaml_checkers = ['yamllint']
    " let g:syntastic_c_config_file = '.syntastic_*_config'
    " let g:syntastic_c_include_dirs = [ '../include', 'include' ]
    " let g:syntastic_debug=1
    let g:ycm_register_as_syntastic_checker = 0
    let g:syntastic_c_checkers=['gcc']


" Enable autocompletion:
    set wildmode=longest,list,full
    let g:ycm_autoclose_preview_window_after_insertion = 1


" Enable nerdtree when no file specified
    autocmd StdinReadPre * let s:std_in=1
    autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Nerd tree open with ctrl + n
    map <C-n> :NERDTreeToggle<CR>

" Tagbar open with ctrl + shift + t
    map <C-S-t> :TagbarToggle<CR>

    " autocmd ColorHighlight
    " let g:colorizer_auto_color = 1
    set mouse+=a

" set tab to spaces
    " show existing tab with 4 spaces width
    set tabstop=4
    " when indenting with '>', use 4 spaces width
    set shiftwidth=4
    " On pressing tab, insert 4 spaces
    set expandtab
    filetype plugin indent on

    " noremap <Leader>y "*y
    " noremap <Leader>p "*p
    " noremap <Leader>Y "+y
    " noremap <Leader>P "+p
    " set clipboard=unnamedplus
